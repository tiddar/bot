import nonebot
import bot.config
from os import path
from datetime import datetime
from multiprocessing import freeze_support

if __name__ == '__main__':
    freeze_support()
    nonebot.init(config_object=bot.config)
    nonebot.load_plugins(path.join(path.dirname(__file__), 'message'), 'message')
    nonebot.run(host='0.0.0.0', port=8080)
